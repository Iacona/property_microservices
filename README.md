# Property_Microservices

Un utilisateur peut modifier les caractéristiques d'un bien (changer le nom, ajouter une pièce, etc… ) ainssi que consulter uniquement les biens d'une ville particulière grace à ce service

## Installation


```bash
git clone [ce repository]
python3 app.py
```

## Usage

grace à un logiciel de gestion d'API (comme postman) vous pouvez tester les diferentes routes
- http://127.0.0.1:5000/addpropriety en PUT request pour modifier un bien avec les arguments (id, name, owner, city, number_of_room)
- http://127.0.0.1:5000/getpropriety en GET request pour afficher une liste des biens d'une ville avec l'argument (city)

```bash
sqlite3 user.db
>> SELECT * from propriety;
```

pour voir le contenu de la table
