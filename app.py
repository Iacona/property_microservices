from flask import Flask
from flask import request
from flask import jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql.sqltypes import Integer
import sqlite3 as sql

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///propriety.db'
db = SQLAlchemy(app)

db.Model.metadata.reflect(db.engine)

class Propriety(db.Model):
    __tablename__ = 'propriety'
    __table_args__ = { 'extend_existing': True }
    id = db.Column(Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    owner = db.Column(db.String(80), nullable=False)
    city = db.Column(db.String(80), nullable=False)
    number_of_room = db.Column(db.Integer, nullable=False)

@app.route("/")
def hello():
    return "Hello World!"

@app.route('/propriety',methods = ['PUT', 'POST', 'GET'])
def propriety():
    if request.method == 'PUT':
        try:
            propriety_id = request.args.get('id')
            name = request.args.get('name')
            owner = request.args.get('owner')
            city = request.args.get('city')
            number_of_room = request.args.get('number_of_room')
         
            with sql.connect("propriety.db") as con:
                cur = con.cursor()
                query = "UPDATE propriety SET name = '" + name + "', owner = '" + owner + "', city = '" + city + "', number_of_room = " + number_of_room + " WHERE id == " + propriety_id
                cur.execute(query)
            
                con.commit()
                msg = "Record successfully added"
        except:
            con.rollback()
            msg = "error in insert operation"
      
        finally:
            con.close()
            return msg

    if request.method == 'POST':
        name = request.args.get('name')
        owner = request.args.get('owner')
        city = request.args.get('city')
        number_of_room = request.args.get('number_of_room')

        propriety = Propriety()
        propriety.name = name
        propriety.city = city
        propriety.number_of_room = int(number_of_room)
        propriety.owner = owner


        db.session.add(propriety)
        db.session.commit()

        msg = "Record successfully added"
        return msg
    
    if request.method == 'GET':
        try:
            city = request.args.get('city')
         
            with sql.connect("propriety.db") as con:
                cur = con.cursor()
                query = "SELECT * FROM propriety WHERE city = '" + city + "'"
                cur.execute(query)
                rows = cur.fetchall()
                
                msg = rows
        except:
            con.rollback()
            msg = "error in get operation"
      
        finally:
            con.close()
            return jsonify(msg)

if __name__ == '__main__':
    app.run(debug=True)